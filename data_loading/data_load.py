"""Functions to load the data"""

import pandas as pd 




def tabular_data_load(X_path, Y_path):
    """Loads the tabular data"""
    X = pd.read_csv(X_path)
    Y = pd.read_csv(Y_path)
    return X, Y
 

def load_tab_image_data(X_img_path, X_tab_path, Y_path):
    """Aggregates the image features data to the tabular data"""
    X_tab, Y = tabular_data_load(X_tab_path, Y_path)                                                  
    X_img = pd.read_csv(X_img_path)
    X = X_tab.merge(right=X_img, how='left', on='id_annonce')
    return X, Y