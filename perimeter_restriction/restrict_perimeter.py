



class PerimeterRestrictor():

    def __init__(self):
        return None
       
    def restrict(self, X, Y):
        X = X.loc[(X['property_type'] =='appartement') \
                | (X['property_type'] =='maison')]
        Y = Y.loc[X.index]
        indexes = X.loc[(X['nb_bathrooms']>X['nb_rooms'])].index
        indexes = indexes.append(X.loc[(X['nb_bedrooms']>X['nb_rooms'])].index)
        indexes = indexes.append(X.loc[X['nb_bedrooms']>6].index)
        indexes = indexes.append(X.loc[X['nb_rooms']>10].index)
        indexes = indexes.append(X.loc[X['size']>400].index)
        indexes = indexes.append(Y.loc[(Y['price']<20000) \
                                      |(Y['price']>2000000)].index)
        X = X.drop(indexes)
        Y = Y.drop(indexes)
        return X, Y