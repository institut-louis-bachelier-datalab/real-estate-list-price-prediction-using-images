"""The class defining the benchmark estimator"""

import numpy as np
import pandas as pd
from IPython.display import display
from sklearn.base import BaseEstimator
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import r2_score
from catboost import CatBoostRegressor




class BenchmarkEstimator(BaseEstimator):
    """Defines the benchmark estimator predicting listing price from tabular 
    features"""
    
    def __init__(self, 
                learning_rate=0.01, 
                subsample=0.1, 
                reg_lambda=5, 
                n_estimators=500, 
                depth=3) -> None:
        super().__init__()
        self.columns = []
        self.learning_rate =learning_rate
        self.subsample = subsample
        self.reg_lambda = reg_lambda
        self.n_estimators = n_estimators
        self.depth = depth
        self.estimator=None
        self.df_results = pd.DataFrame()
    
    def fit(self, x_train, y_train):
        self.columns = x_train.columns
        y_train = np.log1p(y_train)
        reg = CatBoostRegressor(cat_features=['property_type'],
                                loss_function='MAPE',
                                learning_rate = self.learning_rate,
                                subsample = self.subsample,
                                reg_lambda = self.reg_lambda,
                                n_estimators = self.n_estimators,
                                depth = self.depth
                                )
        reg.fit(x_train, y_train)
        self.estimator = reg
        display(reg)
        
    def predict(self, x_test): 
        pred = self.estimator.predict(x_test)
        pred = np.exp(pred)
        return pred
    
    def get_scores(self, y_test, y_pred): 
        id_annonce = y_test['id_annonce']
        y_true = y_test['price']
        mae = mean_absolute_error(y_true, y_pred)
        mdae = median_absolute_error(y_true, y_pred)
        mse = mean_squared_error(y_true, y_pred)
        mape = mean_absolute_percentage_error(y_true, y_pred)
        errors = pd.Series(y_true.to_numpy()) - pd.Series(y_pred)
        relative_errors = (errors/pd.Series(y_true.to_numpy())).abs()
        mdape = relative_errors.median()
        r_squared = r2_score(y_true, y_pred)
        scores = pd.DataFrame({'mae': mae,
                               'mdae': mdae,
                               'mse': mse,
                               'mape': mape,
                               'mdape': mdape,
                               'r_squared':r_squared}, 
                               index=[0])
        self.df_results['id_annonce'] = id_annonce    
        self.df_results['true_price'] = y_true      
        self.df_results['y_pred'] = y_pred                
        self.df_results['error'] = errors
        self.df_results['relative_errors'] = relative_errors
        return scores

    def get_reults_dataframe(self):
        return self.df_results

    def feature_importance(self):
        feat_import = self.estimator.feature_importances_
        return feat_import
    

    




        