"""Class to define the pipeline"""

import pickle

from sklearn.base import BaseEstimator
from sklearn.model_selection import RandomizedSearchCV
from sklearn.pipeline import Pipeline
from modeling.benchmark.tabular_preprocess import TabularPreprocess
from modeling.benchmark.benchmark_estimator import BenchmarkEstimator




class BenchmarkModel(BaseEstimator):
    """Defines the benchmark pipeline"""
    
    def __init__(self, col_outliers, feat_to_drop, dict_PCA_cols):
        super().__init__()
        self.col_outliers = col_outliers
        self.feat_to_drop = feat_to_drop
        self.dict_PCA_cols = dict_PCA_cols
        preprocessor = TabularPreprocess(self.col_outliers,
                                         self.feat_to_drop,
                                         self.dict_PCA_cols)
        estimator =  BenchmarkEstimator()
        pipe = Pipeline(steps=[('preprocess', preprocessor), 
                               ('estimator', estimator)])
        self.pipeline = pipe
        self.searchCV = None
    
    def optimize(self, search_space, X_train, Y_train):
        score='neg_mean_absolute_percentage_error' 
        searchCV = RandomizedSearchCV(self.pipeline,
                                      search_space, 
                                      n_iter=100,
                                      scoring=score,
                                      n_jobs=-1, 
                                      cv=5,
                                      verbose=1)
        searchCV.fit(X_train, Y_train)
        self.searchCV = searchCV
        self.pipeline =  searchCV.best_estimator_
        self.preprocessor = self.pipeline[0]
        self.estimator = self.pipeline[1]

    def predict(self, X_test):
        y_pred = self.pipeline.predict(X_test)
        return y_pred
     
    def get_scores(self, y_test, y_pred):
        return self.estimator.get_scores(y_test, y_pred)
    
    def get_reults_dataframe(self):
        return self.estimator.df_results
    
    def save_pipeline(self, file_path):
        pickle.dump(self, open(file_path, 'wb'))
    
    

