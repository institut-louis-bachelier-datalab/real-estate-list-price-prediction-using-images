"""Class to preprocess the tabular data"""

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.base import BaseEstimator, TransformerMixin




class TabularPreprocess(BaseEstimator, TransformerMixin):
    """Applies the tabular data preprocessing procedure"""
     
    def __init__(self, 
                 col_with_outliers,
                 feat_to_drop, 
                 dict_PCA_cols
                 ) -> None:
        super().__init__()
        self.after_encod_columns = []
        self.upper_quatiles = {}
        self.col_with_outliers = col_with_outliers
        self.feat_to_drop = feat_to_drop
        self.dict_PCA_cols = dict_PCA_cols
        self.PCAs = {}
        
    def fit(self, X, Y=None):
        #X = self._regroup_property_type(X)
        X = self._impute_missing_values(X)
        self._get_quantiles(X)
        self._PCA_fit(X)
        return self
  
    def transform(self, X, Y=None):
        #df = self._regroup_property_type(X)
        df = self._impute_missing_values(X)
        df = self._trimming_outliers(df)
        df = self._PCA_transform(df)
        df = self._features_selection(df)
        return df
    
    #this function could be used before the data restriction
    def _regroup_property_type(self, X): 
        type_shares = X['property_type'].value_counts(normalize = True)
        minor_types = type_shares[type_shares<0.01].index.tolist()
        X['property_type'] = X['property_type'].replace(minor_types, 'autre')
        return X
    
    def _missing_values_by_prop_type(self, X, feat):
        X[feat] = X.groupby('property_type')[feat]\
                     .transform(lambda x: x.fillna(x.median()))
    
    def _impute_missing_values(self, X):
        # the nb_bedrooms is missing for all the property type chambre,
        # so we will set it to 1
        X.loc[X['property_type']=='chambre','nb_bedrooms']=1

        feats_to_impute = ['size', 
                           'nb_bathrooms',
                           'nb_rooms', 
                           'nb_bedrooms']
        for feat in feats_to_impute:
            self._missing_values_by_prop_type(X, feat)

        X['land_size'] = X.apply(
            lambda row: row['size'] if np.isnan(row['land_size'])
                                    else row['land_size'],
            axis=1
        )
        X['energy_performance_value'] = X['energy_performance_value']\
            .fillna(X['energy_performance_value'].median())
        X['ghg_value'] = X['ghg_value']\
            .fillna(X['ghg_value'].median())

        X['floor'] = X['floor'].fillna(0)
        return X

    def _get_quantiles(self, X):
        for col in self.col_with_outliers:
            self.upper_quatiles[col] = X[col].quantile(0.99)

    def _trimming_outliers(self, X):
        for up_out in self.upper_quatiles.keys():
            funct = lambda x : self.upper_quatiles[up_out] \
                            if x>self.upper_quatiles[up_out] else x
            X[up_out] = X[up_out].apply(funct)
        return X 
    
    def _PCA_fit(self, X):
        for col in self.dict_PCA_cols.keys():
            pca = PCA(n_components=1)
            pca.fit(X[self.dict_PCA_cols[col]])
            self.PCAs[col] = pca
    
    def _PCA_transform(self, X):
        for col in self.dict_PCA_cols.keys():
            X[col] = self.PCAs[col].transform(X[self.dict_PCA_cols[col]])
            X = X.drop(labels=self.dict_PCA_cols[col], axis=1)
        return X

    def _features_selection(self, X):
        X = X.drop(labels=self.feat_to_drop, axis=1)
        return X        
    
      
class TabCategEncoder(BaseEstimator, TransformerMixin):
    """Encodes categorical features"""
    
    def __init__(self):
        self.after_encod_columns = []
    
    def fit(self, X, Y=None):
        self._get_after_encod_columns(X)
        return self
    
    def transform(self, X, Y=None):
        df = self.encoding_property_type(X)
        return df
    
    def _get_after_encod_columns(self, X):
        df = X.copy()
        df_dummies = pd.get_dummies(df, columns=['property_type'])
        self.after_encod_columns = df_dummies.columns
    
    def encoding_property_type(self, X):
        X = pd.get_dummies(X, columns=['property_type'])
        diff = set(self.after_encod_columns).difference(set(X.columns))
        if diff != set():
            for col in diff:
                X[col] = 0
        return X




