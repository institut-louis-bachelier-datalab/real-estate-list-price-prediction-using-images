
"""Defining the paths for datasets, models saving and models results saving"""

X_TAB_TRAIN_PATH = '../3_output/restricted_data/X_train_restrict.csv'
Y_TRAIN_PATH = '../3_output/restricted_data/Y_train_restrict.csv'

X_TAB_TEST_PATH = '../3_output/restricted_data/X_test_restrict.csv'
Y_TEST_PATH = '../3_output/restricted_data/Y_test_restrict.csv'

IMAGE_TRAIN_PATH = "D:/Projects/Immobilier/Travaux sur les images/1_input/images/train"
IMAGE_TEST_PATH = "D:/Projects/Immobilier/Travaux sur les images/1_input/images/test"

ENRICHED_FEATURES_TRAIN_SAVING_PATH =  '../3_output/image_feature_extraction/train/Features_enriched_train.csv'
ENRICHED_FEATURES_TEST_SAVING_PATH =  '../3_output/image_feature_extraction/test/Features_enriched_trest.csv'

AGGREGATED_FEATURES_TRAIN_SAVING_PATH =  '../3_output/image_feature_extraction/train/Features_aggregated_train.csv'
AGGREGATED_FEATURES_TEST_SAVING_PATH =  '../3_output/image_feature_extraction/test/Features_aggregated_test.csv'

IMAGE_EMBEDDINGS_TRAIN_SAVING_PATH = '../3_output/image_embeddings/image_embeddings_train.csv'
IMAGE_EMBEDDINGS_TEST_SAVING_PATH = '../3_output/image_embeddings/image_embeddings_test.csv'

IMAGE_FILES_NAMES_TRAIN_PATH = '../3_output/image_file_names/image_file_names_train.csv'
IMAGE_FILES_NAMES_TEST_PATH = '../3_output/image_file_names/image_file_names_test.csv'

train_data_paths = {'images' : IMAGE_TRAIN_PATH, 
                    'enriched_features' : ENRICHED_FEATURES_TRAIN_SAVING_PATH,
                    'aggregated_features' : AGGREGATED_FEATURES_TRAIN_SAVING_PATH,
                    'image_files_names' : IMAGE_FILES_NAMES_TRAIN_PATH,
                    'listing_img_embeddings' : IMAGE_EMBEDDINGS_TRAIN_SAVING_PATH,
                    'x_tab' : X_TAB_TRAIN_PATH,
                    'y' : Y_TRAIN_PATH }

test_data_paths = {'images' : IMAGE_TEST_PATH,
                   'enriched_features' : ENRICHED_FEATURES_TEST_SAVING_PATH,
                   'aggregated_features' : AGGREGATED_FEATURES_TEST_SAVING_PATH,
                   'image_files_names' : IMAGE_FILES_NAMES_TEST_PATH,
                   'listing_img_embeddings' : IMAGE_EMBEDDINGS_TEST_SAVING_PATH,
                   'x_tab' : X_TAB_TEST_PATH,
                   'y' : Y_TEST_PATH }

#models_saving_paths
BENCHMARK_SAVING_PATH = '../3_output/models/benchmark.sav'
EXTRACTED_FEATURES_REGRESSOR_SAVING_PATH = '../3_output/models/regressor_with_extracted_img_feat.sav'
IMAGE_EMBEDDINGS_REGRESSOR_SAVING_PATH = '../3_output/models/regressor_with_image_embeddings.sav'
IMAGE_PER_IMAGE_NETWORK_SAVING_PATH = '../3_output/models/image_per_image_network/'
IMAGE_NETWORK_SAVING_PATH = '../3_output/models/image_network/'
TABULAR_NETWORK_SAVING_PATH = '../3_output/models/tabular_network/'
MIXED_MODEL_SAVING_PATH = '../3_output/models/mixed_model/'

#models_results_saving_paths
RESULTS_BENCHMARK_SAVING_PATH =  '../3_output/models_results/benchmark_results.csv'
RESULTS_EXTRACTED_FEATURES_REGRESSOR_SAVING_PATH =  '../3_output/models_results/regressor_with_extracted_img_features_results.csv'
RESULTS_IMAGE_EMBEDDINGS_REGRESSOR_SAVING_PATH = '../3_output/models_results/regressor_with_image_embeddings_results.csv'

#models_saving_logs
LOGS_IMAGE_PER_IMAGE_NETWORK_SAVING_PATH = '../3_output/logs_image_per_image_network.txt'
LOGS_IMAGE_NETWORK_SAVING_PATH = '../3_output/logs_image_network.txt'
LOGS_TABULAR_NETWORK_SAVING_PATH = '../3_output/logs_tabular_network.txt'
LOGS_MIXED_MODEL_SAVING_PATH = '../3_output/logs_mixed_model.txt'

models_saving_paths = {'benchmark' : BENCHMARK_SAVING_PATH,
                       'extracted_features_regressor' : EXTRACTED_FEATURES_REGRESSOR_SAVING_PATH,
                       'image_embeddings_regressor' : IMAGE_EMBEDDINGS_REGRESSOR_SAVING_PATH,
                       'image_per_image_network' : IMAGE_PER_IMAGE_NETWORK_SAVING_PATH,
                       'image_network' : IMAGE_NETWORK_SAVING_PATH,
                       'tabular_network' : TABULAR_NETWORK_SAVING_PATH,
                       'mixed_model' : MIXED_MODEL_SAVING_PATH}

models_results_saving_paths = {'benchmark': RESULTS_BENCHMARK_SAVING_PATH,
                              'extracted_features_regressor' : RESULTS_EXTRACTED_FEATURES_REGRESSOR_SAVING_PATH, 
                              'image_per_image_network' : RESULTS_IMAGE_EMBEDDINGS_REGRESSOR_SAVING_PATH}

models_results_saving_logs = {'image_per_image_network' : LOGS_IMAGE_PER_IMAGE_NETWORK_SAVING_PATH,
                              'image_network' : LOGS_IMAGE_NETWORK_SAVING_PATH, 
                              'tabular_network' : LOGS_TABULAR_NETWORK_SAVING_PATH,
                              'mixed_model' : LOGS_MIXED_MODEL_SAVING_PATH}

