"""Classes to define the datasets"""

import os

import pandas as pd
import numpy as np
from PIL import Image
from sklearn import preprocessing
import torch 
from torchvision import transforms
from torch.utils.data import Dataset

from modeling.benchmark.tabular_preprocess import (TabularPreprocess, 
                                                   TabCategEncoder)
from modeling.parameters import (tab_col_outliers, 
                                 tab_feat_drop,
                                 tab_PCA_dict,
                                 )



                                          
class ImagesTabDataset(Dataset):
    """Generates the dataset with the tabular and the image features"""
    
    def __init__(self, tab_file_path, targets_file_path, img_dir):
        self.img_dir = img_dir
        self.nb_photos = 6
        tab_prep = TabularPreprocess(tab_col_outliers, 
                                     tab_feat_drop, 
                                     tab_PCA_dict)
        tab_encoder = TabCategEncoder()
        tab_data = tab_prep.fit_transform(pd.read_csv(tab_file_path))
        tab_data  = tab_encoder.fit_transform(tab_data)
        cols = tab_data.columns
        binairy_cols = ['has_a_balcony', 
                        'has_a_cellar', 
                        'has_a_garage', 
                        'has_air_conditioning',
                        'property_type_appartement',
                        'property_type_maison']
        feat_to_scale = set(cols).difference(set(binairy_cols))
        scaler = preprocessing.StandardScaler()
        scaled = pd.DataFrame(scaler.fit_transform(tab_data[feat_to_scale]),
                              columns= feat_to_scale)
        self.df_tab = pd.concat([scaled, tab_data[binairy_cols]], axis=1)
        self.df_targets = pd.read_csv(targets_file_path)
        self.target_scaler = preprocessing.StandardScaler()\
                            .fit((self.df_targets['price'])\
                            .to_numpy().reshape(-1, 1)) 
    
    def preprocess_images(self, image_paths):
        preprocess = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                                             std=[0.229, 0.224, 0.225]),
                    ])
        image_tensors = [preprocess(Image.open(f)) for f in image_paths]
        return torch.stack(image_tensors)     
        
    def __len__(self):
        return len(self.df_targets)

    def __getitem__(self, index):
        id_annonce = self.df_targets.loc[index]['id_annonce']
        ann_path = os.path.join(self.img_dir, 'ann_' + str(int(id_annonce)))
        img_paths = [os.path.join(ann_path, f) for f in os.listdir(ann_path)\
                     if f.split('.')[-1] == 'jpg']
        ann_images = self.preprocess_images(img_paths)
        while (ann_images.shape[0]<6):
            ann_images = torch.cat((ann_images, 
                                    ann_images[0:6-ann_images.shape[0],]),
                                    dim=0)
        tab_feat = torch.tensor(self.df_tab.loc[index].values\
                                                      .astype(np.float32))
        target = torch.tensor(
                        self.target_scaler.transform(
                            self.df_targets.loc[index]['price'].reshape(-1, 1)
                            )
                        ).view(1)
        return tab_feat, ann_images, target
    
    
class ImagePerImageSet(Dataset):
    """Generates the dataset containing the collection of individual images"""
    
    def __init__(self, image_file_names_path, targets_file_path, img_dir):
        self.img_dir = img_dir
        targets = pd.read_csv(targets_file_path)
        df_image_files_names = pd.read_csv(image_file_names_path)
        self.df_data = pd.merge(df_image_files_names, targets, on='id_annonce')
        self.target_scaler = preprocessing.StandardScaler()\
                            .fit((self.df_data['price'])\
                            .to_numpy().reshape(-1, 1)) 
    
    def preprocess_images(self, image_path):
        preprocess = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                                             std=[0.229, 0.224, 0.225]),
                    ])
        return preprocess(Image.open(image_path))        
        
    def __len__(self):
        return len(self.df_data)

    def __getitem__(self, index):
                          
        id_annonce = self.df_data.loc[index]['id_annonce']  
        img_path = os.path.join(self.img_dir, 
                               'ann_'+ str(self.df_data['id_annonce'][index]), 
                                str(self.df_data['file_name'][index]))
        image = self.preprocess_images(img_path)
        target = torch.tensor(
                        self.target_scaler.transform(
                            self.df_data.loc[index]['price'].reshape(-1, 1)
                            )
                        ).view(1)
        return id_annonce, image, target