from time import time

import torch
import numpy as np 

from torch.utils.tensorboard import SummaryWriter
tensorboard_writer = SummaryWriter()
        
        
def save_batch_scores(self, epoch, step, targets, preds, loss, t0_batch):
    """"""
    preds = self.target_scaler.inverse_transform(preds.detach().numpy())
    targets = self.target_scaler.inverse_transform(targets.detach().numpy())
    for score_function in self.score_functions:
        score = score_function(targets, preds)
        score_details = {
            'timestamp' : time(),
            'train_or_val' : 'train',
            'epoch' : epoch,
            'step' : step,
            'score_function' : score_function.__name__,
            'score' : score,
            'elapsed_time' : time() - t0_batch,
        }
        tensorboard_writer.add_scalar("train/" + score_function.__name__ + "/", 
                                      score, 
                                      step + epoch*self.n_steps_per_epoch)
        self.scores.append(score_details)
        self.log_score_details(score_details)
   
    
def save_batch_loss(self, epoch, step, loss, t0_batch):
    """"""
    loss_details = {
        'timestamp' : time(),
        'train_or_val' : 'train',
        'epoch' : epoch,
        'step' : step,
        'score_function' : 'loss',
        'score' : loss,
        'elapsed_time' : time() - t0_batch,
    }
    tensorboard_writer.add_scalar("train/loss/", 
                                  loss, 
                                  step + epoch*self.n_steps_per_epoch)
    self.scores.append(loss_details)
    self.log_score_details(loss_details)
    
    
def save_epoch_scores(self, epoch, t0_epoch, train_or_val = 'val'):
    """Saves the epoch scores"""
    self.model.eval()
    scores = {s.__name__ : [] for s in self.score_functions}
    scores['loss'] = []
    
    for step, batch in enumerate(self.data_loaders[train_or_val]) :
        features, images, targets = batch[0].to(self.device), \
                                    batch[1].to(self.device), \
                                    batch[2].to(self.device)
        with torch.no_grad():
            preds = self.model((features, images)) 
            
        loss = self.criterion(preds.view(-1).float(), targets.view(-1).float())
        scores['loss'].append(loss.item())
        
        preds = self.target_scaler.inverse_transform(preds.detach().numpy())
        targets = self.target_scaler.inverse_transform(targets.detach().numpy())
        for score_function in self.score_functions:
            score = score_function(targets, preds)
            scores[score_function.__name__].append(score)
            
    for score_function_key in scores:
        score = np.mean(scores[score_function_key])
        score_details = {
            'timestamp' : time(),
            'train_or_val' : train_or_val,
            'epoch' : epoch,
            'score_function' : score_function_key,
            'score' : score,
            'elapsed_time' : time() - t0_epoch,
        }      
        tensorboard_writer.add_scalar(train_or_val + "_epoch/" \
                                        + score_function_key + "/", 
                                    score, 
                                    epoch)
        self.scores.append(score_details)
        self.log_score_details(score_details)


def initialise_log_file(self):
    """"""
    with open(self.log_file, 'w') as f:
        f.write('[\n')
        
        
def log_score_details(self, score_details):
    """"""
    with open(self.log_file, 'a') as f:
        f.write(str(score_details))
        f.write(', \n')
        
        
def finalise_log_file(self):
    """"""
    with open(self.log_file, 'a') as f:
        f.write(']')