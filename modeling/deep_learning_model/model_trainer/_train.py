from time import time

import torch

from torch.utils.tensorboard import SummaryWriter
tensorboard_writer = SummaryWriter()


def train(self, save_models = True):
    """"""
    self.initialise_log_file()
    
    for epoch in range(self.epochs):
        t0_epoch = time()
        self.model.train()
        
        for step, batch in enumerate(self.data_loaders['train']):
            t0_batch = time()
            features, images, targets = batch[0].to(self.device), \
                                        batch[1].to(self.device), \
                                        batch[2].to(self.device)
            self.optimizer.zero_grad()
            preds = self.model((features, images))
            loss = self.criterion(preds.view(-1).float(), 
                                targets.view(-1).float())
            loss.backward()
            self.optimizer.step()
            self.save_batch_loss(epoch, step, loss.item(), t0_batch)
            self.save_batch_scores(epoch, step, targets, preds, loss.item(), 
                                   t0_batch)
            
        self.scheduler.step()
        self.save_epoch_scores(epoch, t0_epoch, train_or_val = 'train')
        self.save_epoch_scores(epoch, t0_epoch, train_or_val = 'val')
        if save_models: 
            torch.save(self.model.state_dict(), self.model_export_path \
                                               + self.model.__class__.__name__ \
                                               + '_trained_epoch_' + str(epoch))
    self.finalise_log_file()