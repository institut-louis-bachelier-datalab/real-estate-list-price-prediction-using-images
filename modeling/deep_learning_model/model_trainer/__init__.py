import abc
from sklearn.metrics import (mean_absolute_percentage_error, 
                             mean_absolute_error, 
                             r2_score)




SCORE_FUNCTIONS = [
    mean_absolute_percentage_error, 
    mean_absolute_error, 
    r2_score
]


class BaseModelTrainer(abc.ABC):
    """Abstract template class for training the different models"""
    
    def __init__(self, model, data_loaders, optimizer, criterion, scheduler,
                epochs, batch_size, device, target_scaler,
                score_functions = SCORE_FUNCTIONS, log_file = './logs.txt',
                model_export_path = './'):
        
        self.model = model
        self.epochs = epochs
        self.batch_size = batch_size
        self.device = device
        self.data_loaders = data_loaders
        self.optimizer = optimizer
        self.criterion = criterion
        self.scheduler = scheduler
        self.target_scaler = target_scaler
        self.score_functions = score_functions
        self.n_steps_per_epoch = len(data_loaders['train'])
        self.scores = []
        self.log_file = log_file
        self.model_export_path = model_export_path

    def __str__(self):
        return str('ModelTrainer')
    
    @abc.abstractmethod
    def train(self):
        pass
    
    # Imported methods
    from ._logging import save_batch_loss
    from ._logging import save_batch_scores
    from ._logging import save_epoch_scores
    from ._logging import initialise_log_file
    from ._logging import finalise_log_file
    from ._logging import log_score_details
    
    
    
    
class ModelTrainer(BaseModelTrainer):
    """Class to train models at lisitng level"""
    
    from ._train import train
    
    
class ImagePreTrainer(BaseModelTrainer):
    """Class to train image per image model"""
    
    from ._pretrain import train
    from ._pretrain import save_image_per_image_epoch_scores
    