from time import time

import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter
tensorboard_writer = SummaryWriter()


def train(self, save_models = True):
    """"""
    self.initialise_log_file()
    
    for epoch in range(self.epochs):
        t0_epoch = time()
        self.model.train()
        
        for step, batch in enumerate(self.data_loaders['train']):
            t0_batch = time()
            images, targets = batch[1].to(self.device), batch[2].to(self.device)
            self.optimizer.zero_grad()
            preds = self.model(images)
            loss = self.criterion(preds.view(-1).float(), 
                                targets.view(-1).float())
            loss.backward()
            self.optimizer.step()
            self.save_batch_loss(epoch, step, loss.item(), t0_batch)
            self.save_batch_scores(epoch, step, targets, preds, loss.item(), 
                                   t0_batch)
            
        self.scheduler.step()
        self.save_image_per_image_epoch_scores(epoch, t0_epoch, 
                                               train_or_val = 'train')
        self.save_image_per_image_epoch_scores(epoch, t0_epoch, 
                                               train_or_val = 'val')
        if save_models: 
            torch.save(self.model.state_dict(), self.model_export_path \
                                               + self.model.__class__.__name__ \
                                               + '_trained_epoch_' + str(epoch))
    self.finalise_log_file()
    
    
def save_image_per_image_epoch_scores(self, epoch, t0_epoch, 
                                      train_or_val = 'val'):
    """"""
    self.model.eval()
    scores = {s.__name__ : [] for s in self.score_functions}
    scores['loss'] = []
    
    for step, batch in enumerate(self.data_loaders[train_or_val]) :
        images, targets = batch[1].to(self.device), batch[2].to(self.device)
        with torch.no_grad():
            preds = self.model(images)
            
        loss = self.criterion(preds.view(-1).float(), targets.view(-1).float())
        scores['loss'].append(loss.item())
        
        preds = self.target_scaler.inverse_transform(preds.detach().numpy())
        targets = self.target_scaler.inverse_transform(targets.detach().numpy())
        for score_function in self.score_functions:
            score = score_function(targets, preds)
            scores[score_function.__name__].append(score)
                
    for score_function_key in scores:
        score = np.mean(scores[score_function_key])
        score_details = {
            'timestamp' : time(),
            'train_or_val' : train_or_val,
            'epoch' : epoch,
            'score_function' : score_function_key,
            'score' : score,
            'elapsed_time' : time() - t0_epoch,
        }      
        tensorboard_writer.add_scalar(train_or_val + "_epoch/" \
                                        + score_function_key + "/", 
                                    score, 
                                    epoch)
        self.scores.append(score_details)
        self.log_score_details(score_details)
        