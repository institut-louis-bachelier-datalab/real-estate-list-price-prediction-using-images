"""Defining the different deep learning models"""

import torch 
import torch.nn as nn

from modeling.deep_learning_model.network_architectures import (
                                                        img_embedder,
                                                        img_regressor,
                                                        tab_regressor,
                                                        mixed_data_regressor
                                                        )




class MixedDataNetwork(nn.Module):
    """Estimates listing prices from both tabular and image features"""

    def __init__(self, 
                 img_embedder=img_embedder, 
                 img_regressor=img_regressor, 
                 tab_regressor=tab_regressor, 
                 requires_grad=False):
        super(MixedDataNetwork, self).__init__()        
        self.img_embedder = img_embedder
        self.img_regressor = img_regressor              
        self.tab_regressor = tab_regressor
        for branch in [self.img_embedder, 
                       self.img_regressor, 
                       self.tab_regressor]:
            for p in branch.parameters():
                p.requires_grad = requires_grad
        self.mixed_data_regressor = mixed_data_regressor

    def forward(self, inputs):
        tab_inputs = inputs[0]
        img_inputs = inputs[1] 
        image_embeddings = self._embed_images(img_inputs)
        img_outputs = self.img_regressor(image_embeddings)
        tab_outputs = self.tab_regressor(tab_inputs)
        concat_outputs = torch.cat((img_outputs, tab_outputs),1)
        return self.mixed_data_regressor(concat_outputs)

    def _embed_images(self, img_inputs_batch):
        embeddings = [] 
        for img_input in img_inputs_batch:
            img_embeddings = img_embedder(img_input)
            embeddings.append(img_embeddings)       
        return torch.mean(torch.stack(embeddings), 1)[:, :, 0, 0]
    

class TabularNetwork(nn.Module):
    """Estimates listing prices from tabular features"""

    def __init__(self):
        super(TabularNetwork, self).__init__()        
        self.tab_regressor = tab_regressor
        self.final_layer = nn.Sequential(nn.Linear(8, 1))

    def forward(self, inputs):
        tab_inputs = inputs[0]
        tab_outputs = self.tab_regressor(tab_inputs)
        return self.final_layer(tab_outputs)
    

class ImageNetwork(nn.Module):
    """Estimates listing prices from image features (listing level)"""

    def __init__(self, img_embedder=img_embedder, requires_grad=False):
        super(ImageNetwork, self).__init__()        
        self.img_embedder = img_embedder
        for p in self.img_embedder.parameters():
            p.requires_grad = requires_grad
        self.img_regressor = img_regressor
        self.final_layer = nn.Sequential(nn.Linear(12, 1))

    def forward(self, inputs):
        img_inputs_batch = inputs[1] 
        image_embeddings = self._embed_images(img_inputs_batch)
        image_embeddings = self.img_regressor(image_embeddings)
        return self.final_layer(image_embeddings)

    def _embed_images(self, img_inputs_batch):
        """"""
        embeddings = [] 
        for img_input in img_inputs_batch:
            img_embeddings = img_embedder(img_input)
            embeddings.append(img_embeddings)       
        return torch.mean(torch.stack(embeddings), 1).squeeze()


class ImagePerImageNetwork(nn.Module):
    """Estimates listing prices from image features (image level)"""

    def __init__(self, requires_grad = False):
        super(ImagePerImageNetwork, self).__init__()
        self.img_embedder = img_embedder
        for i in range(6,9):
            for p in self.img_embedder[i].parameters():
                p.requires_grad = requires_grad
        for p in self.img_embedder.parameters():
            p.requires_grad = requires_grad
        self.img_regressor = img_regressor
        self.final_layer = nn.Sequential(nn.Linear(12, 1))

    def forward(self, image_inputs):
        image_embeddings = self.img_embedder(image_inputs).squeeze()
        image_embeddings = self.img_regressor(image_embeddings)
        return self.final_layer(image_embeddings)