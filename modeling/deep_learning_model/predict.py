"""Functions to get the predictions and the scores of the deep learning models"""

import pandas as pd
import torch
from sklearn.metrics import (mean_absolute_error,
                             median_absolute_error,
                             mean_squared_error,
                             mean_absolute_percentage_error,
                             r2_score)




def predict_from_mixed_model(model, 
                             dataloader, 
                             device, 
                             target_scaler):
    """Returns the price predictions of the mixed model"""
    df_prices = pd.DataFrame({#'id_annonce' : [],
                              'true_price' : [],
                              'predicted_price' : []})
    for step, batch in enumerate(dataloader) :
        tab_inputs, img_inputs, targets = batch[0].to(device), \
                                          batch[1].to(device), \
                                          batch[2].to(device)                              
        inputs = [tab_inputs, img_inputs]
        with torch.no_grad():
           preds = target_scaler.inverse_transform(model(inputs)).round(2)
        batch_prices = pd.DataFrame({#'id_annonce' : tab_inputs['id_annoce']\
                                     #                    .squeeze().tolist(),
                                      'true_price': targets.squeeze().tolist(),
                                      'predicted_price': preds.squeeze()\
                                                              .tolist()})
        df_prices = pd.concat([batch_prices, df_prices], 
                               ignore_index=True)
    return df_prices
    

def predict_listing_embeddings(embeddings, 
                               model, 
                               dataloader, 
                               device, 
                               target_scaler):
    """Returns the prediction of image embeddings (listing level)"""
    embeddings = predict_embeddings_per_image(model, 
                                              dataloader, 
                                              target_scaler,
                                              device 
                                            )
    listing_embeds_groups = pd.DataFrame(embeddings.groupby('id_annonce'))                                       
    all_listings_embeddings = [] 
    for i in range(len(listing_embeds_groups)):
        listing_embedding = three_medians_aggregation(
                                        listing_embeds_groups[1][i]\
                                            ['img_embedding'])
        all_listings_embeddings.append(listing_embedding)
    df_listing_embeds= pd.DataFrame({'id_annonce' : listing_embeds_groups[0], 
                                     'embeddings': all_listings_embeddings})
    return df_listing_embeds


def three_medians_aggregation(list):
    """Returns the mean of the 3 meadians of a list"""
    df = pd.DataFrame({'embeddings': list})
    df['ecart'] = abs(df['embeddings'] - df['embeddings'].median())
    three_medians = df.nsmallest(3, 'ecart')['embeddings']
    return (three_medians.mean()).round(2)


def predict_embeddings_per_image(model, dataloader, device, target_scaler):  
    """Returns the prediction of image embeddings (image level)"""  
    img_embeddings = pd.DataFrame({'id_annonce' : [],
                                   'img_embedding' : []})
    for step, batch in enumerate(dataloader) :
        id_ann, images = batch[0].to(device), batch[1].to(device)

        with torch.no_grad():
           preds = target_scaler.inverse_transform(model(images)).round(2)

        batch_embeddings = pd.DataFrame({'id_annonce' : id_ann.squeeze()\
                                                              .tolist(),
                                         'img_embedding': preds.squeeze()\
                                                               .tolist()})
        img_embeddings = pd.concat([img_embeddings, batch_embeddings], 
                                    ignore_index=True)
    return img_embeddings


def get_scores(df_predictions): 
    """"Returns the scores for the regression task
    Args:
        df_predictions (dataframe): contains a true price column and predicted
                                    price column.
    """
    y_true = df_predictions['true_price']
    y_pred = df_predictions['predicted_price']
    mae = mean_absolute_error(y_true, y_pred)
    mdae = median_absolute_error(y_true, y_pred)
    mse = mean_squared_error(y_true, y_pred)
    mape = mean_absolute_percentage_error(y_true, y_pred)
    errors = pd.Series(y_true.to_numpy()) - pd.Series(y_pred)
    relative_errors = (errors/pd.Series(y_true.to_numpy())).abs()
    mdape = relative_errors.median()
    r_squared = r2_score(y_true, y_pred)
    scores = pd.DataFrame({'mae': mae,
                           'mdae': mdae,
                           'mse': mse,
                           'mape': mape,
                           'mdape': mdape,
                           'r_squared':r_squared}, 
                           index=[0])
    return scores


