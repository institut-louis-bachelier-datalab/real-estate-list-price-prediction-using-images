"""This file contains the defintion of the neural network architectures"""

import torch.nn as nn
from torchvision.models import resnet34
from torchvision.models import ResNet34_Weights



resnet = resnet34(weights=ResNet34_Weights.DEFAULT)

img_embedder = nn.Sequential(*list(resnet.children())[:-1])
                  
img_regressor = nn.Sequential(
                    nn.Linear(512, 256),
                    nn.BatchNorm1d(256),
                    nn.LeakyReLU(inplace=True),
                    nn.Dropout(0.2),

                    nn.Linear(256, 128),
                    nn.BatchNorm1d(128),
                    nn.LeakyReLU(inplace=True),
                    nn.Dropout(0.2),

                    nn.Linear(128, 32),
                    nn.BatchNorm1d(32),
                    nn.LeakyReLU(inplace=True),
                    nn.Dropout(0.2),
                        
                    nn.Linear(32, 12),
                    nn.BatchNorm1d(12),
                    nn.LeakyReLU(inplace=True)
                    )
                      
                                                               
tab_regressor = nn.Sequential(
                    nn.Linear(17, 32),    
                    nn.BatchNorm1d(32),
                    nn.ReLU(inplace=True),
                    
                    nn.Linear(32, 16),
                    nn.BatchNorm1d(16),
                    nn.ReLU(inplace=True),
                    
                    nn.Linear(16, 8),
                    nn.BatchNorm1d(8),
                    nn.ReLU(inplace=True),
                    )



mixed_data_regressor = nn.Sequential(
                    nn.Linear(20, 32),
                    nn.ReLU(inplace=True),
                    nn.BatchNorm1d(32),
                    
                    nn.Linear(32, 16),
                    nn.ReLU(inplace=True),
                    
                    nn.Linear(16, 8),
                    nn.ReLU(inplace=True),
                    nn.BatchNorm1d(8),

                    nn.Linear(8, 1),
                    )