"""Defining the parameters"""

tab_col_outliers = ['size',
                    'energy_performance_value',
                    'ghg_value',
                    'nb_rooms',
                    'nb_bedrooms']

tab_feat_drop = ['id_annonce', 
                 'nb_photos',
                 'postal_code',
                 'last_floor',
                 'upper_floors',
                 'city',
                 'exposition',
                 #'ghg_value', 
                 #'energy_performance_value',  
                 'ghg_category', 
                 'energy_performance_category']

rooms_cols = ['nb_rooms',
              'nb_bedrooms',
              'nb_bathrooms']  
              
tab_PCA_dict = {'nb_rooms_projected': rooms_cols}

PCA_cols_orientation = ['orientation_paysage',
                        'orientation_portrait',
                        'orientation_carre']

PCA_mean_colors = ['mean_B', 'mean_G', 'mean_R']
PCA_skew_R = ['skew_catg_R_negative', 'skew_catg_R_positive']
PCA_skew_B = ['skew_catg_B_negative', 'skew_catg_B_positive']
PCA_skew_G = ['skew_catg_G_negative', 'skew_catg_G_positive']

img_PCA_dict = {'orientation' : PCA_cols_orientation, 
                'mean_colors' : PCA_mean_colors,
                'skew_catg_R' : PCA_skew_R,
                'skew_catg_G' : PCA_skew_G,
                'skew_catg_B' : PCA_skew_B 
                }
img_feat_drop = ['aspect_ratio_autre',
                 'main_color',
                 'skew_catg_R_symmetrical', 
                 'skew_catg_G_symmetrical',
                 'skew_catg_B_symmetrical']

search_space = {'estimator__learning_rate': (0.001, 0.01, 0.01, 0.1, 0.5, 1),
                'estimator__subsample': (0.1, 0.4, 0.6, 0.8, 1),
                'estimator__reg_lambda': (1e-9, 5, 10, 20),
                'estimator__n_estimators': (100, 300, 500, 750, 1000),
                'estimator__depth': (3, 6, 7, 8, 9)
                }