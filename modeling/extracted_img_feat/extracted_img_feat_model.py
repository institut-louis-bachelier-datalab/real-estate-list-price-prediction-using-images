"""Class to define the extracted image features model"""

import pickle

from sklearn.base import BaseEstimator
from sklearn.pipeline import Pipeline
from sklearn.model_selection import RandomizedSearchCV

from modeling.benchmark.tabular_preprocess import TabularPreprocess
from modeling.extracted_img_feat.img_preprocess import ImageFeaturePreprocess
from modeling.benchmark.benchmark_estimator import BenchmarkEstimator




class ExtractedImageFeatModel(BaseEstimator):
    """Estimates the listing price from the tabular data combined with the 
    extracted image features """

    def __init__(self,
                 tab_outliers_col, 
                 tab_feat_drop, 
                 tab_PCA_col, 
                 img_PCA_col,
                 img_feat_drop):
        super().__init__()
        self.tab_outliers_col = tab_outliers_col
        self.tab_feat_drop = tab_feat_drop
        self.tab_PCA_col = tab_PCA_col
        self.img_feat_drop = img_feat_drop
        self.img_PCA_col = img_PCA_col
        tab_preprocessor = TabularPreprocess(self.tab_outliers_col,
                                             self.tab_feat_drop,
                                             self.tab_PCA_col)
        img_preprocessor = ImageFeaturePreprocess(self.img_PCA_col,
                                                  self.img_feat_drop)
        estimator =  BenchmarkEstimator()
        pipe = Pipeline(steps=[('tab_preprocessor', tab_preprocessor), 
                               ('img_preprocessor', img_preprocessor), 
                               ('estimator', estimator)])
        self.pipeline = pipe
        self.searchCV = None

    
    def optimize(self, search_space, X_train, Y_train):
        score='neg_mean_absolute_percentage_error' 
        searchCV = RandomizedSearchCV(self.pipeline,
                                      search_space, 
                                      n_iter=100,
                                      scoring=score,
                                      n_jobs=-1, 
                                      cv=5,
                                      verbose=1)
        searchCV.fit(X_train, Y_train)
        self.searchCV = searchCV
        self.pipeline =  searchCV.best_estimator_
        self.tab_preprocessor = self.pipeline[0]
        self.img_preprocessor = self.pipeline[1]
        self.estimator = self.pipeline[2]

    def predict(self, X_test):
        y_pred = self.pipeline.predict(X_test)
        return y_pred
     
    def get_scores(self, y_test, y_pred):
        return self.estimator.get_scores(y_test, y_pred)
    
    def get_reults_dataframe(self):
        return self.estimator.df_results

    def save_pipeline(self, file_path):
        pickle.dump(self, open(file_path, 'wb'))


    
    

