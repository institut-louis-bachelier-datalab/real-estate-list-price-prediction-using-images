"""Class to preprocess the extracted image features"""

from sklearn.decomposition import PCA
from sklearn.base import BaseEstimator, TransformerMixin




class ImageFeaturePreprocess(BaseEstimator, TransformerMixin):
    """Applies the preprocessing procedure on the extracted image features"""
    
    def __init__(self, dict_PCA_cols, feat_to_drop) -> None:
        super().__init__()
        self.feat_to_drop = feat_to_drop
        self.dict_PCA_cols = dict_PCA_cols
        self.PCAs = {}
        
    def fit(self, X, Y=None):
        self._PCA_fit_all(X)
        return self
  
    def transform(self, X, Y=None):
        X = self._PCA_transform_all(X)
        X = self._features_selection(X)
        return X

    def _PCA_fit_all(self, X):
        for col in self.dict_PCA_cols.keys():
            pca = PCA(n_components=1)
            pca.fit(X[self.dict_PCA_cols[col]])
            self.PCAs[col]=pca
    
    def _PCA_transform_all(self, X):
        for col in self.dict_PCA_cols.keys():
            X[col] = self.PCAs[col].transform(X[self.dict_PCA_cols[col]])
            X = X.drop(labels=self.dict_PCA_cols[col], axis=1)
        return X

    def _features_selection(self, X):
        X = X.drop(labels=self.feat_to_drop, axis=1)
        return X