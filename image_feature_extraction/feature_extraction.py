"""Classes to extract simple interpretable features from listing images"""

import os

import numpy as np
import pandas as pd
import cv2
from scipy.stats import skew

from image_feature_extraction.hsv_feature_extraction import HsvImageExtractor




class ImageExtractor:
    """Extracts features from a given image"""
    
    def __init__(self, image_path):
        self._image_path = image_path
        self._image = cv2.imread(image_path)
        self._features = {}
    
    def _get_file_name(self, path):
        head, tail = os.path.split(path)
        self._features['file_name'] = tail or os.path.basename(head)
         
    def _is_color(self):  
        verify_01 = np.array_equal(self._image[:,:,0], self._image[:,:,1])
        verify_12 = np.array_equal(self._image[:,:,1], self._image[:,:,2])
        self._features['is_color'] = not(verify_01 & verify_12)

    def _get_dimensions(self):
        self._features['height'] =  self._image.shape[0]
        self._features['width'] =  self._image.shape[1]
        self._features['channels'] =  self._image.shape[2]
    
    def _get_color_moments(self):
        self._features['mean_B'] = self._image[:,:,0].mean()
        self._features['mean_G'] = self._image[:,:,1].mean()
        self._features['mean_R'] = self._image[:,:,2].mean()
        self._features['var_B'] = self._image[:,:,0].var()
        self._features['var_G'] = self._image[:,:,1].var()
        self._features['var_R'] = self._image[:,:,0].var()
        self._features['skew_B'] = skew(self._image[:,:,0], axis=None)
        self._features['skew_G'] = skew(self._image[:,:,1], axis=None)
        self._features['skew_R'] = skew(self._image[:,:,2], axis=None)

    def _get_hsv_features(self):
        hsv_features = HsvImageExtractor(self._image).extract_features()
        self._features.update(hsv_features)
    
    def extract_features(self):
        self._get_file_name(self._image_path)
        self._is_color()
        self._get_dimensions()
        self._get_color_moments()
        self._get_hsv_features()
        return self._features
    
    
class ListingExtractor:
    """Extracts features from the images in the listing folder"""
    
    def __init__(self, folder_path):
        self._folder_path = folder_path
        self._annonce_id = folder_path.split('ann_')[-1]
        self._image_paths = self._get_image_paths(folder_path)
        self._image_count = len(self._image_paths)
        self._features = None
        
    def _get_image_paths(self, folder_path):
        return [os.path.join(folder_path, f) for f in os.listdir(folder_path) \
                if f.split('.')[-1] == 'jpg']
    
    def _extract_image_features(self):
        features_dict = [ImageExtractor(f).extract_features() \
            for f in self._image_paths]
        self._features = pd.DataFrame(features_dict)
    
    def _add_listing_level_features(self):
        self._features.insert(0, 'id_annonce', self._annonce_id)
        self._features.insert(1, 'nb_donwloaded_photos', self._image_count)
    
    def extract_features(self):
        self._extract_image_features()
        self._add_listing_level_features()
        return self._features
        
          
class FeatureExtractor:
    """Extracts features from all the listing folders and images"""
    
    def __init__(self, folder_path):
        self._folder_path = folder_path
        self._listing_folders = [os.path.join(folder_path, f) \
            for f in os.listdir(folder_path)]
        self._features = None
    
    def extract_features(self):
        features_dfs = []
        for listing_folder in self._listing_folders:
            features_df = ListingExtractor(listing_folder).extract_features()
            features_dfs.append(features_df)
        self._features = pd.concat(features_dfs)
        return self._features


    
    
                                  
            
         