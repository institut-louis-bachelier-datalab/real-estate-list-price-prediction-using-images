"""Classes to generate new features from the features extracted from the images"""

import pandas as pd




class FeatureEnricher:
    """Adds new features to the image level features"""
    
    
    def __init__(self, img_feat):
        self._feat_photo = img_feat 

    def _get_orientation(self):
        diff = self._feat_photo['height'] -  self._feat_photo['width']
        funct = lambda x : 'portrait' if x>0 \
                       else ('paysage' if x<0 else 'carre')
        self._feat_photo['orientation'] = diff.apply(funct)

    
    def _get_aspect_ratios(self):
        aspect_ratio = self._feat_photo['width']/self._feat_photo['height']
        funct = lambda x : '1:1' if x==1/1 \
                       else ('3:2' if x==3/2  
                                   else ('4:3' if x==4/3 
                                               else ('16:9' if x == 16/9
                                                            else 'autre')))
        self._feat_photo['aspect_ratio'] = aspect_ratio.apply(funct)

    def top_n_from_row(self, row, k):
        return set(row.nlargest(k).index)

    def _get_main_color(self,colors):
        self._feat_photo['main_color'] = self._feat_photo[colors]\
                                             .idxmax(axis="columns")

    def _get_top_two_main_colors(self,colors):
        self._feat_photo['top_two_colors'] = self._feat_photo[colors]\
                                        .apply(self.top_n_from_row, axis=1, k=2)
                                                                    
    def _get_least_colors(self, colors):
        self._feat_photo['least_color'] = self._feat_photo[colors]\
                                             .idxmin(axis="columns")

    def _get_main_least_colors(self):
        colors = ['red', 'magenta', 'yellow', 'cyan', 'blue', 'green']
        self._get_main_color(colors)
        self._get_top_two_main_colors(colors)
        self._get_least_colors(colors)
    
    def _get_skew_to_categ_one_channel(self, channel):
        skew_channel = self._feat_photo['skew_'+channel]
        funct = lambda x : 'positive' if x>0 \
                       else ('negative' if x<0 else 'symmetrical')
        self._feat_photo['skew_catg_'+channel] = skew_channel.apply(funct) 

    def  _get_skew_to_categ_all_channels(self):
        channels = ['R', 'G', 'B']
        for channel in channels:
            self._get_skew_to_categ_one_channel(channel)
    
    def enrich_features(self):
        self._get_orientation()
        self._get_aspect_ratios()
        self._get_main_least_colors()
        self._get_skew_to_categ_all_channels()
        return self._feat_photo
    
    def save_csv(self, saving_path):
        (self._feat_photo).to_csv(saving_path, index =False)




class FeatureAggregator:
    """Aggregates image level features to listing level features"""

    def __init__(self, img_feat):
        self._feat_photo = img_feat
        ids = self._feat_photo['id_annonce'].unique()
        self._feat_annonce = pd.DataFrame({'id_annonce':ids})
    
    def _nbr_photos_scrapped_by_annonce(self):
        features_groupby = self._feat_photo[['id_annonce', 'file_name']]\
                            .groupby(['id_annonce'])
        file_name_counts = features_groupby.count().reset_index()
        nbr_photos_scrapped = pd.DataFrame(file_name_counts)
        nbr_photos_scrapped.rename(columns={"file_name": "nb_photos_scrapped"}, 
                                    inplace=True)
        self._feat_annonce = pd.merge(self._feat_annonce, nbr_photos_scrapped)

    def _main_color_by_annonce(self):
        main_color = self._feat_photo.groupby(by='id_annonce').main_color\
                                                .agg(pd.Series.mode)
        self._feat_annonce = pd.merge(self._feat_annonce,
                                      main_color,
                                      on='id_annonce')
        
    def _mean_feature_by_annonce(self, feat):
        feature_groupby = self._feat_photo[['id_annonce', feat]]\
                            .groupby(['id_annonce'])
        feature_mean = pd.DataFrame(feature_groupby[feat].mean())
        self._feat_annonce = pd.merge(self._feat_annonce,
                                      feature_mean,
                                      on='id_annonce') 

    def _feature_counts_by_annonce(self, feature):
        feature_df = self._feat_photo[['id_annonce', feature]]
        feature_dummies = pd.get_dummies(data=feature_df)
        feature_groupby = feature_dummies.groupby(by='id_annonce')
        feature_counts = pd.DataFrame(feature_groupby.sum())
        self._feat_annonce = pd.merge(self._feat_annonce, 
                                      feature_counts, 
                                      on='id_annonce')

    def _skew_all_channels_by_annonce(self): 
        channels = ['R', 'G', 'B']
        for channel in channels:
            self._feature_counts_by_annonce('skew_catg_'+channel)
    
    def _counts_to_ratio_by_annonce(self):
        count_to_ratio_feat = ['orientation_carre',
       'orientation_paysage', 'orientation_portrait', 'aspect_ratio_16:9',
       'aspect_ratio_1:1', 'aspect_ratio_3:2', 'aspect_ratio_4:3',
       'aspect_ratio_autre', 'main_color_blue', 'main_color_cyan',
       'main_color_green', 'main_color_magenta', 'main_color_red',
       'main_color_yellow', 'skew_catg_R_negative', 'skew_catg_R_positive',
       'skew_catg_R_symmetrical', 'skew_catg_G_negative',
       'skew_catg_G_positive', 'skew_catg_G_symmetrical',
       'skew_catg_B_negative', 'skew_catg_B_positive',
       'skew_catg_B_symmetrical']
        for feat in count_to_ratio_feat: 
            self._feat_annonce[feat] = (self._feat_annonce[feat]\
                                    /self._feat_annonce['nb_photos_scrapped'])\
                                    .round(2)
            #self._feat_annonce.rename(columns={feat: feat+'_ratio'}, 
            #                          inplace=True)

    def aggregate_features(self):
        self._nbr_photos_scrapped_by_annonce()
        self._feature_counts_by_annonce('orientation')
        self._feature_counts_by_annonce('aspect_ratio')
        self._feature_counts_by_annonce('main_color')
        self._main_color_by_annonce()
        feat_to_mean = ['brillance', 'saturation', 'mean_B', 'mean_G', 'mean_R']
        for feat in feat_to_mean :
            self._mean_feature_by_annonce(feat)
        self._skew_all_channels_by_annonce()
        self._counts_to_ratio_by_annonce()
        return self._feat_annonce

    def save_csv(self, saving_path):
        (self._feat_annonce).to_csv(saving_path, index =False)
    
        