"""Class to extract features from HSV image representation"""

import pandas as pd
import cv2




class HsvImageExtractor:
    """Extracts features from an HSV image"""
    def __init__(self, image):
        self._hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        self._color_segments = {
            0 : 'red', 
            1 : 'yellow', 
            2 : 'green', 
            3 : 'cyan', 
            4 : 'blue', 
            5 : 'magenta',
        }
        self._features = {}
    
    def _get_hue_histogram(self): # Slow function, could be optimised ?
        color_codes = ((self._hsv_image[0,:,:]+20)%180)//30
        color_code_counts = pd.Series(color_codes.flatten()).value_counts(normalize = True)
        color_counts = color_code_counts.rename(index = self._color_segments).to_dict()
        for color in self._color_segments.values() : 
            if color not in color_counts:
                color_counts[color] = 0
        self._features.update(color_counts)
    
    def _get_mean_saturation(self):
        saturations = self._hsv_image[:,:,1].flatten()
        self._features['saturation'] = saturations.mean()
    
    def _get_mean_brillance(self):
        brillances = self._hsv_image[:,:,2].flatten()
        self._features['brillance'] = brillances.mean()
                
    def extract_features(self):
        self._get_hue_histogram()
        self._get_mean_saturation()
        self._get_mean_brillance()
        return self._features